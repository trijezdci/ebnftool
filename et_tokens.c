/* EBNF tool
 *
 *  @file et_tokens.c
 *  token implementation
 *
 *  Lexical analysis for EBNF grammar files
 *
 *  Author: Benjamin Kowarsch
 *
 *  Copyright (C) 2009 Benjamin Kowarsch. All rights reserved.
 *
 *  License:
 *
 *  Redistribution  and  use  in source  and  binary forms,  with  or  without
 *  modification, are permitted provided that the following conditions are met
 *
 *  1) NO FEES may be charged for the provision of the software.  The software
 *     may  NOT  be  hosted  on websites  which  contain  advertising,  unless
 *     specific  prior  written  permission has been obtained.
 *
 *  2) Redistributions  of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *  3) Redistributions  in binary form  must  reproduce  the  above  copyright
 *     notice,  this list of conditions  and  the following disclaimer  in the
 *     documentation and other materials provided with the distribution.
 *
 *  4) Neither the author's name nor the names of any contributors may be used
 *     to endorse  or  promote  products  derived  from this software  without
 *     specific prior written permission.
 *
 *  5) Where this list of conditions  or  the following disclaimer, in part or
 *     as a whole is overruled  or  nullified by applicable law, no permission
 *     is granted to use the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,  BUT NOT LIMITED TO,  THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY  AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT  SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE  FOR  ANY  DIRECT,  INDIRECT,  INCIDENTAL,  SPECIAL,  EXEMPLARY,  OR
 * CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE,  DATA,  OR PROFITS; OR BUSINESS
 * INTERRUPTION)  HOWEVER  CAUSED  AND ON ANY THEORY OF LIABILITY,  WHETHER IN
 * CONTRACT,  STRICT LIABILITY,  OR TORT  (INCLUDING NEGLIGENCE  OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,  EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *  
 *  Version history:
 *
 *   1.00   2009-09-25   BK   new file
 */


#include "et_tokens.h"


// ---------------------------------------------------------------------------
// Human readable string representations of tokens
// ---------------------------------------------------------------------------

static const char _illegal_char_str[] = "illegal character\0";
static const char _uppercase_name_str[] = "uppercase-name\0";
static const char _lowercase_name_str[] = "lowercase-name\0";
static const char _literal_str[] = "literal\0";
static const char _colon_str[] = "\":\"\0";
static const char _equals_str[] = "\"=\"\0";
static const char _semicolon_str[] = "\";\"\0";
static const char _vertical_bar_str[] = "\"|\"\0";
static const char _question_mark_str[] = "\"?\"\0";
static const char _plus_str[] = "\"+\"\0";
static const char _asterisk_str[] = "\"*\"\0";
static const char _opening_parenthesis_str[] = "\"(\"\0";
static const char _closing_parenthesis_str[] = "\")\"\0";
static const char _dotdot_str[] = "\"..\"\0";
static const char _end_of_file_str[] = "end-of-file\0";
static const char _empty_str[] = "\0";


// ---------------------------------------------------------------------------
// Array of human readable string representations
// ---------------------------------------------------------------------------

static const char *_token_name_str[] = {
    (const char *) &_illegal_char_str,
    (const char *) &_uppercase_name_str,
    (const char *) &_lowercase_name_str,
    (const char *) &_literal_str,
    (const char *) &_colon_str,
    (const char *) &_equals_str,
    (const char *) &_semicolon_str,
    (const char *) &_vertical_bar_str,
    (const char *) &_question_mark_str,
    (const char *) &_plus_str,
    (const char *) &_asterisk_str,
    (const char *) &_opening_parenthesis_str,
    (const char *) &_closing_parenthesis_str,
    (const char *) &_dotdot_str,
    (const char *) &_end_of_file_str
}; // _token_name_str


// ---------------------------------------------------------------------------
// function:  et_token_name( token )
// ---------------------------------------------------------------------------
//
// Returns a human readable string representation of token <token>.

const char *et_token_name(et_token_t token) {
    if (token <= ET_NUMBER_OF_TOKENS)
        return _token_name_str[token];
    else
        return (const char *) &_empty_str;
} // end et_token_name


// END OF FILE
