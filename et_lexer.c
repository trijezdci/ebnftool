/* EBNF tool
 *
 *  @file et_lexer.c
 *  EBNF lexer implementation
 *
 *  Lexical analysis for EBNF grammar files
 *
 *  Author: Benjamin Kowarsch
 *
 *  Copyright (C) 2009 Benjamin Kowarsch. All rights reserved.
 *
 *  License:
 *
 *  Redistribution  and  use  in source  and  binary forms,  with  or  without
 *  modification, are permitted provided that the following conditions are met
 *
 *  1) NO FEES may be charged for the provision of the software.  The software
 *     may  NOT  be  hosted  on websites  which  contain  advertising,  unless
 *     specific  prior  written  permission has been obtained.
 *
 *  2) Redistributions  of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *  3) Redistributions  in binary form  must  reproduce  the  above  copyright
 *     notice,  this list of conditions  and  the following disclaimer  in the
 *     documentation and other materials provided with the distribution.
 *
 *  4) Neither the author's name nor the names of any contributors may be used
 *     to endorse  or  promote  products  derived  from this software  without
 *     specific prior written permission.
 *
 *  5) Where this list of conditions  or  the following disclaimer, in part or
 *     as a whole is overruled  or  nullified by applicable law, no permission
 *     is granted to use the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,  BUT NOT LIMITED TO,  THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY  AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT  SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE  FOR  ANY  DIRECT,  INDIRECT,  INCIDENTAL,  SPECIAL,  EXEMPLARY,  OR
 * CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE,  DATA,  OR PROFITS; OR BUSINESS
 * INTERRUPTION)  HOWEVER  CAUSED  AND ON ANY THEORY OF LIABILITY,  WHETHER IN
 * CONTRACT,  STRICT LIABILITY,  OR TORT  (INCLUDING NEGLIGENCE  OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,  EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *  
 *  Version history:
 *
 *   1.00   2009-09-25   BK   new file
 */


// ---------------------------------------------------------------------------
// Standard library imports
// ---------------------------------------------------------------------------

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

// ---------------------------------------------------------------------------
// Project imports
// ---------------------------------------------------------------------------

#include "KVS.h"
#include "ASCII.h"
#include "et_build_params.h"
#include "et_hash.h"
#include "et_lexer.h"


// ---------------------------------------------------------------------------
// Lexeme buffer size
// ---------------------------------------------------------------------------

#define ET_MAX_LEXEME_LENGTH 128


// ---------------------------------------------------------------------------
// Lexeme buffer type
// ---------------------------------------------------------------------------

typedef struct /* lexbuf_t */ {
    cardinal length;
    char string[ET_MAX_LEXEME_LENGTH];
} lexbuf_t;


// ---------------------------------------------------------------------------
// Lexer state type
// ---------------------------------------------------------------------------

typedef /* et_lexer_s */ struct {
    
    // configuration parameters
    FILE *sourcefile;                   // source file
    kvs_table_t lextab;              // lexeme table
    
    // return values
    et_token_t token;                   // token to be returned
    cardinal lexkey;                    // lexeme key to be returned
    et_lexer_status_t status;           // status to be returned
    
    // counters
    file_pos_t token_pos;               // position of current symbol
    file_pos_t current_pos;             // position of current character
    uint16_t paren_nesting_level;       // current parenthesis nesting level
    uint16_t bracket_nesting_level;     // current bracket nesting level
    uint16_t brace_nesting_level;       // current brace nesting level
    
    // flags
    bool end_of_file;                   // end-of-file has been reached
    
    // lexeme buffer
    lexbuf_t lexeme;
    
    // offending character
    char offending_char;
    file_pos_t offending_char_pos;
} et_lexer_s;

#define NOT_EOF(_lexer) (_lexer->end_of_file == false)
#define EOF_REACHED(_lexer) (_lexer->end_of_file == true)


// ==========================================================================
// P R I V A T E   F U N C T I O N   P R O T O T Y P E S
// ==========================================================================

static fmacro uchar_t _readchar(et_lexer_s *lexer);

static fmacro uchar_t _nextchar(et_lexer_s *lexer);

static fmacro uchar_t get_ident(et_lexer_s *lexer);

static fmacro uchar_t get_quoted_literal(et_lexer_s *lexer);

static fmacro uchar_t get_escaped_char(et_lexer_s *lexer);

static fmacro void add_lexeme_to_lextab(et_lexer_s *lexer);

static fmacro uchar_t skip_c_comment(et_lexer_s *lexer);

static fmacro uchar_t skip_past_end_of_line(et_lexer_s *lexer);


// ==========================================================================
// P U B L I C   F U N C T I O N   I M P L E M E N T A T I O N S
// ==========================================================================

#define readchar(v) _readchar(this_lexer) /* v = void */
#define nextchar(v) _nextchar(this_lexer) /* v = void */

// --------------------------------------------------------------------------
// function:  et_new_lexer(infile, lextab, status)
// --------------------------------------------------------------------------
//
// Creates  and  returns  a  new  lexer object  associated  with  source file 
// <infile> and lexeme table <lextab>.  The status of the operation is passed
// back in <status> unless NULL is passed in for <status>.
//
// Returns NULL if the lexer object could not be created.

et_lexer_t et_new_lexer(FILE *infile,
                 kvs_table_t lextab,
           et_lexer_status_t *status) {
    
    et_lexer_s *new_lexer;    
    
    // assert pre-conditions
    
    if (infile == NULL) {
        ASSIGN_BY_REF(status, ET_LEXER_STATUS_INVALID_REFERENCE);
        return NULL;
    } // end if
    
    errno = 0;
    rewind(infile);
    if (errno != 0) {
        ASSIGN_BY_REF(status, ET_LEXER_STATUS_INVALID_REFERENCE);
        return NULL;
    } // end if
    
    if (lextab == NULL) {
        ASSIGN_BY_REF(status, ET_LEXER_STATUS_INVALID_REFERENCE);
        return NULL;
    } // end if
    
    // allocate a new lexer object
    new_lexer = (et_lexer_s) malloc(sizeof(et_lexer_s));
    
    if (new_lexer == NULL) {
        ASSIGN_BY_REF(status, ET_LEXER_STATUS_ALLOCATION_FAILED);
        return NULL;
    } // end if
    
    // initialise the new lexer object
    
    // configuration parameters
    new_lexer->sourcefile = infile;
    new_lexer->lextab = lextab;
    
    // return values
    new_lexer->token = 0;
    new_lexer->lexkey = 0;
    
    // counters
    SET_FPOS(new_lexer->token_pos, 1, 1);
    SET_FPOS(new_lexer->current_pos, 1, 1);
    new_lexer->paren_nesting_level = 0;
    new_lexer->bracket_nesting_level = 0;
    new_lexer->brace_nesting_level = 0;
    
    // lexer flags
    new_lexer->end_of_file = false;
    
    // input buffer
    new_lexer->lexeme.length = 0;
    new_lexer->lexeme.string[0] = CSTRING_TERMINATOR;
    
    // offending character
    new_lexer->offending_char = 0;
    SET_FPOS(new_lexer->offending_char_pos, 0, 0);
    
    // return the initialised lexer object
    ASSIGN_BY_REF(status, ET_LEXER_STATUS_SUCCESS);
    return (et_lexer_t) new_lexer;
} // end et_new_lexer;


// ---------------------------------------------------------------------------
// function:  et_lexer_getsym(lexer, lexeme, status)
// ---------------------------------------------------------------------------
//
// Reads one symbol from the input stream of lexer <lexer>, returns its token,
// and passes a key for its lexeme back in <lexeme> unless  NULL  is passed in
// for <lexeme>.  The  status  of  the  operation  is  passed back in <status>
// unless NULL is passed in for <status>.

et_token_t et_lexer_getsym(et_lexer_t lexer,
                             cardinal *lexeme
                    et_lexer_status_t *status) {
    
    register et_lexer_s *this_lexer = (et_lexer_s *) lexer;
    register uchar_t ch;
    bool ignore_token;
    
    // assert pre-condition
    if (lexer == NULL) {
        ASSIGN_BY_REF(lexeme, 0);
        ASSIGN_BY_REF(status, ET_LEXER_STATUS_INVALID_REFERENCE);
        return TOKEN_ILLEGAL_CHARACTER;
    } // end if
    
    // clear lexeme
    this_lexer->lexkey = 0;
    this_lexer->lexeme.length = 0;
    this_lexer->lexeme.string[0] = CSTRING_TERMINATOR;
    
    ch = nextchar();
    ignore_token = false;
    repeat {
        
        // skip all whitespace, tab and EOL characters
        while ((NOT_EOF(this_lexer)) &&
               ((ch == WHITESPACE) || (ch == TAB) || (ch == EOL))) {
            // skip the current character
            readchar();
            // take a peek at the next one
            ch = nextchar();
        } // end while;
        
        // remember position at the start of the symbol
        this_lexer->token_pos = this_lexer->current_pos;
        
        // start optimistically
        this_lexer->status = ET_LEXER_STATUS_SUCCESS;
        
        // check for end-of-file
        if (EOF_REACHED(this_lexer)) {
            this_lexer->token = TOKEN_EOF_MARKER;
        } // end eof check
        
        // check for identifier
        else if ((ch == UNDERSCORE) || (IS_LETTER(ch))) {
            // found identifier
            ch = get_ident(this_lexer);
            if (this_lexer->status == ET_LEXER_STATUS_SUCCESS)
                add_lexeme_to_lextab(this_lexer);                
        } // end identifier check
        
        else switch (ch) {
            case SINGLE_QUOTE :
            case DOUBLE_QUOTE :
                ch = get_quoted_literal(this_lexer);
                if (this_lexer->status == ET_LEXER_STATUS_SUCCESS)
                    add_lexeme_to_lextab(this_lexer);                
                break;
            case DOT :
                ch = readchar();
                ch = nextchar();
                if (ch == DOT) { // found '..'
                    ch = readchar();
                    ch = nextchar();
                    this_lexer->token = TOKEN_DOTDOT;
                }
                else {
                    this_lexer->offending_char = readchar();
                    this_lexer->offending_char_pos = this_lexer->current_pos;
                    ch = nextchar();
                    this_lexer->token = TOKEN_ILLEGAL_CHARACTER;
                } // end if
                break;
            case COLON :
                ch = readchar();
                ch = nextchar();
                if (ch == EQUAL_SIGN) { // found ':='
                    ch = readchar();
                    ch = nextchar();
                    this_lexer->token = TOKEN_ASSIGN_OP;
                }
                else {
                    this_lexer->token = TOKEN_COLON;
                } // end if
                break;
            case SEMICOLON :
                ch = readchar();
                ch = nextchar();
                this_lexer->token = TOKEN_SEMICOLON;
                break;
            case OPENING_PARENTHESIS :
                ch = readchar();
                ch = nextchar();
                this_lexer->token = TOKEN_OPENING_PARENTHESIS;
                break;
            case CLOSING_PARENTHESIS :
                ch = readchar();
                ch = nextchar();
                this_lexer->token = TOKEN_CLOSING_PARENTHESIS;
                break;
            case TILDE :
                ch = readchar();
                ch = nextchar();
                this_lexer->token = TOKEN_LOGICAL_NOT_OP;
                break;
            case EQUAL_SIGN :
                ch = readchar();
                ch = nextchar();
                this_lexer->token = TOKEN_EQUAL_OP;
                break;
            case QUESTION_MARK :
                ch = readchar();
                ch = nextchar();
                this_lexer->token = TOKEN_OPTIONAL;
                break;
            case PLUS :
                ch = readchar();
                ch = nextchar();
                this_lexer->token = TOKEN_PLUS;
                break;
            case ASTERISK :
                ch = readchar();
                ch = nextchar();
                this_lexer->token = TOKEN_ASTERISK;
                break;
            case FORWARD_SLASH :
                ch = readchar();
                ch = nextchar();
                switch (ch) {
                    case ASTERISK : // found '/*'
                        ch = skip_c_comment(this_lexer);
                        ignore_token = true;
                        break;
                    case FORWARD_SLASH : // found '//'
                        ch = skip_past_end_of_line(this_lexer);
                        ignore_token = true;
                        break;
                    default : // found illegal character
                        this_lexer->offending_char = readchar();
                        this_lexer->offending_char_pos = this_lexer->current_pos;
                        ch = nextchar();
                        this_lexer->token = TOKEN_ILLEGAL_CHARACTER;
                } // end switch
                break;
            case VERTICAL_BAR :
                ch = readchar();
                ch = nextchar();
                this_lexer->token = TOKEN_VERTICAL_BAR;
                break;
            default : // found illegal character
                this_lexer->offending_char = readchar();
                this_lexer->offending_char_pos = this_lexer->current_pos;
                ch = nextchar();
                this_lexer->token = TOKEN_ILLEGAL_CHARACTER;
        } // end if
    } until (ignore_token == false);
    
    // pass back lexeme key and status
    ASSIGN_BY_REF(lexeme, this_lexer->lexkey);
    ASSIGN_BY_REF(status, this_lexer->status);
    
    // return the token
    return this_lexer->token;
} // end et_lexer_getsym;


// ---------------------------------------------------------------------------
// function:  et_lexer_getpos(lexer, row, col, status)
// ---------------------------------------------------------------------------
//
// Obtains the position of the last symbol read from the input stream.  Passes
// the row back in <row>  unless NULL is passed in for <row>,  and the coloumn
// back in <col>  unless  NULL  is  passed  in  for <col>.  The status  of the
// operation is passed back in <status> unless NULL is passed in for <status>.

void e2f_lexer_getpos(et_lexer_t lexer,
                        cardinal *row,
                        cardinal *col,
               et_lexer_status_t *status) {
    
    et_lexer_s *this_lexer = (et_lexer_s *) lexer;
    
    if (lexer == NULL)
        ASSIGN_BY_REF(status, ET_LEXER_STATUS_INVALID_REFERENCE);
    else {
        ASSIGN_BY_REF(row, this_lexer->token_pos.line);
        ASSIGN_BY_REF(col, this_lexer->token_pos.col);
        ASSIGN_BY_REF(status, ET_LEXER_STATUS_SUCCESS);
    } // end if
    
    return;
} // end et_lexer_getpos


// ---------------------------------------------------------------------------
// function:  et_offending_char(lexer, row, col, status)
// ---------------------------------------------------------------------------
//
// Returns the offending character of the last read operation  and  passes its
// position  back  in  <row>  and <col>.  If no error occurred during the last
// read operation then ASCII NUL is returned  and zero is passed pack in <row>
// and <col>.  This function should only be called  after a preceeding call to
// function et_lexer_getsym()  returned  an  error  indicating that an illegal
// or unexcpected character was found.  The status of the operation  is passed
// back in <status> unless NULL is passed in for <status>.

char et_offending_char(e2f_lexer_t lexer,
                          cardinal *row,
                          cardinal *col,
                 et_lexer_status_t *status) {
    
    et_lexer_s *this_lexer = (et_lexer_s *) lexer;
    
    if (lexer == NULL) {
        ASSIGN_BY_REF(row, 0);
        ASSIGN_BY_REF(col, 0);
        ASSIGN_BY_REF(status, ET_LEXER_STATUS_INVALID_REFERENCE);
        return ASCII_NUL;
    }
    else if (this_lexer->status != ET_LEXER_STATUS_ILLEGAL_CHARACTER) {
        ASSIGN_BY_REF(row, 0);
        ASSIGN_BY_REF(col, 0);
        ASSIGN_BY_REF(status, ET_LEXER_STATUS_INVALID_REFERENCE);
        return ASCII_NUL;
    }
    else {
        ASSIGN_BY_REF(row, this_lexer->current_pos.line);
        ASSIGN_BY_REF(col, this_lexer->current_pos.col);
        ASSIGN_BY_REF(status, ET_LEXER_STATUS_SUCCESS);
        return this_lexer->offending_char;
    } // end if
} // end et_offending_char


// ---------------------------------------------------------------------------
// function:  et_dispose_lexer(lexer)
// ---------------------------------------------------------------------------
//
// Disposes of lexer object <lexer>  and  closes its sourcefile if it is open. 
// The  symbol table  used  by  the lexer is  NOT  disposed of.  The status of
// the operation is passed back in <status>.

void et_dispose_lexer(et_lexer_t lexer, et_lexer_status_t *status) {
    
    et_lexer_s *this_lexer = (et_lexer_s *) lexer;
    
    if (lexer == NULL) {
        ASSIGN_BY_REF(status, ET_LEXER_STATUS_INVALID_REFERENCE);
        return;
    } // end if
    
    fclose(this_lexer->sourcefile);
    free(this_lexer);
    
    ASSIGN_BY_REF(status, ET_LEXER_STATUS_SUCCESS);
    return;
} // end et_dispose_lexer;

#undef readchar
#undef nextchar


// ==========================================================================
// P R I V A T E   F U N C T I O N   I M P L E M E N T A T I O N S
// ==========================================================================

// ---------------------------------------------------------------------------
// macros for private functions
// ---------------------------------------------------------------------------

#define readchar(v) _readchar(lexer) /* v = void */
#define nextchar(v) _nextchar(lexer) /* v = void */


// ---------------------------------------------------------------------------
// private function:  _readchar(lexer)
// ---------------------------------------------------------------------------
//
// Reads one character from the input stream of <lexer>  and  returns it.  The
// lexer's coloumn counter is incremented.  Returns linefeed (ASCII LF) if any
// of linefeed (ASCII LF)  or carriage return (ASCII CR)  or  a combination of
// CR and LF (CRLF or LFCR) is read.  If LF is returned,  the  lexer's coloumn
// counter is reset and its line counter is incremented.
//
// pre-conditions:
//  o  lexer is an initialised lexer object
//
// post-conditions:
//  o  new current character is the character read (consumed)
//  o  new lookahead character is the character following the character read
//  o  position counters are updated accordingly
//
// return-value:
//  o  read (consumed) character is returned

static fmacro uchar_t _readchar(e2f_lexer_s *lexer) {
    register int c;
    
#ifndef PRIV_FUNCS_DONT_CHECK_NULL_PARAMS
    if (lexer == NULL) return (uchar_t)0;
#endif
    
    // read one character from source file
    c = getc(lexer->sourcefile);
    
    // handle LF style end-of-line
    if (c == ASCII_LF) {
        lexer->current_pos.col = 1;
        lexer->current_pos.line++;
    }
    // handle CRLF and CR style end-of-line
    else if (c == ASCII_CR) {
        lexer->current_pos.col = 1;
        lexer->current_pos.line++;
        c = getc(lexer->sourcefile);
        if (c != NEWLINE) {
            ungetc(c, lexer->sourcefile);
        } // end if
        c = NEWLINE;
    }
    // handle end-of-file
    else if (c == EOF) {
        // set end-of-file flag if end-of-file reached
        lexer->end_of_file = (feof(lexer->sourcefile) == true);
        c = 0;
    }
    else /* any other characters */ {
        // increment row counter
        lexer->current_pos.col++;
    } // end if
    
    if (((uchar_t) c == 255) || (c == 0)) {
        printf("");
    } // end if
    
    // return character
    return (uchar_t) c;
} // end _readchar


// ---------------------------------------------------------------------------
// private function:  _nextchar(lexer)
// ---------------------------------------------------------------------------
//
// Returns the lookahead character in the input stream of <lexer>  and returns
// it without incrementing the file pointer  and  without changing the lexer's
// coloumn and line counters.
//
// pre-conditions:
//  o  lexer is an initialised lexer object
//
// post-conditions:
//  o  position counters remain unchanged
//
// return-value:
//  o  lookahead character is returned

static fmacro uchar_t _nextchar(e2f_lexer_s *lexer) {
    register int status;
    register int c;
    
#ifndef PRIV_FUNCS_DONT_CHECK_NULL_PARAMS
    if (lexer == NULL) return (uchar_t)0;
#endif
    
    c = getc(lexer->sourcefile);
    
    status = ungetc(c, lexer->sourcefile);
    if (status != EOF) {
        lexer->end_of_file = false;
    }
    else {
        lexer->end_of_file = true;
        c = 0;
    } // end if
    
    return (uchar_t) c;
} // end _nextchar


// ---------------------------------------------------------------------------
// private function:  get_ident(lexer)
// ---------------------------------------------------------------------------
//
// Reads  an  identifier  from  the  input stream of <lexer>  and  returns the
// character following the identifier.
//
// This function accepts input conforming to the following syntax:
//
//  identifier := ( "_" | letter ) ( "_" | letter | digit )*
//  letter := "a" .. "z" | "A" .. "Z"
//  digit := "0" | "1" | "2" | "3" | "4" | "5" | "6" | "7" | "8" | "9"
//
// pre-conditions:
//  o  lexer is an initialised lexer object
//  o  the current lookahead character is the first character at the beginning
//     of the identifier.
//  o  the length of the identifier does not exceed ET_MAX_IDENT_LENGTH.
//
// post-conditions:
//  o  lexer->lexeme.string contains the identifier,
//     followed by a C string terminator (ASCII NUL).
//  o  lexer->lexeme.length contains the length of lexer->lexeme.string.
//  o  lexer->token contains
//     - TOKEN_TERMINAL_IDENTIFIER
//     - TOKEN_NONTERMINAL_IDENTIFIER
//  o  lexer->lexkey contains the key for the lexeme table.
//  o  lexer->status contains ET_LEXER_STATUS_SUCCESS.
//  o  the new lookahead character is the character following the identifier.
//
// error-conditions:
//  if the identifier exceeds ET_MAX_IDENT_LENGTH
//  o  lexer->lexeme.string contains the significant characters only,
//     followed by a C string terminator (ASCII NUL).
//  o  otherwise, post-conditions apply.

static fmacro uchar_t get_ident(et_lexer_s *lexer) {
    cardinal uppercase_char_count = 0, underscore_count = 0;
    uchar_t final_ch = 0;
    uchar_t ch;
    
#ifndef PRIV_FUNCS_DONT_CHECK_NULL_PARAMS
    if (lexer == NULL) return (uchar_t)0;
#endif
    
    lexer->lexeme.length = 0;
    lexer->lexkey = ET_HASH_INITIAL;
    
    ch = readchar();
    if (IS_UPPERCASE(ch))
        uppercase_char_count++;
    else if (ch == UNDERSCORE)
        underscore_count++;
    lexer->lexeme.string[lexer->lexeme.length] = ch;
    lexer->lexkey = ET_HASH_NEXT_CHAR(lexer->lexkey, ch);
    lexer->lexeme.length++;
    ch = nextchar();
    
    while (((IS_ALPHANUM(ch)) || (ch == UNDERSCORE)) &&
           ((lexer->lexeme.length < ET_MAX_IDENT_LENGTH) &&
            (NOT_EOF(lexer)))) {
        ch = readchar();
        if (IS_UPPERCASE(ch))
            uppercase_char_count++;
        else if (ch == UNDERSCORE)
            underscore_count++;
        lexer->lexeme.string[lexer->lexeme.length] = ch;
        lexer->lexkey = ET_HASH_NEXT_CHAR(lexer->lexkey, ch);
        lexer->lexeme.length++;
        ch = nextchar();
    } // while
    
    // any further identifier characters are not significant, skip them
    while (((IS_ALPHANUM(ch)) || (ch == UNDERSCORE)) && (NOT_EOF(lexer))) {
        ch = readchar();
        ch = nextchar();
    } // end while
        
    // terminate the lexeme
    lexer->lexeme.string[lexer->lexeme.length] = CSTRING_TERMINATOR;
    lexer->lexkey = ET_HASH_FINAL(lexer->lexkey);
    
    // determine if terminal or non-terminal identifier
    if (uppercase_char_count + underscore_count == lexer->lexeme.length) {
        lexer->token = TOKEN_TERMINAL_IDENTIFIER;
    }
    else {
        lexer->token = TOKEN_NONTERMINAL_IDENTIFIER;
    } // end if
    
    return ch;
} // end get_ident


// ---------------------------------------------------------------------------
// private function:  get_quoted_literal(lexer)
// ---------------------------------------------------------------------------
//
// Reads a  quoted literal  from the input stream of <lexer>  and  returns the
// character following the literal.
//
// pre-conditions:
//  o  lexer is an initialised lexer object.
//  o  the current lookahead character is the delimiting quotation mark at the
//     beginning of the literal.
//  o  the literal is properly delimited with matching opening and closing
//     quotation marks, does not exceed the maximum string length and does
//     not contain any control characters.
//
// post-conditions:
//  o  lexer->lexeme.string contains the literal including delimiters,
//     followed by a type designator ('A' for ASCII, 'U' for Unicode),
//     followed by a C string terminator (ASCII NUL).
//  o  lexer->lexeme.length contains the length of lexer->lexeme.string.
//  o  lexer->token contains TOKEN_STRING_LITERAL.
//  o  lexer->lexkey contains the key for the lexeme table.
//  o  lexer->status contains ET_LEXER_STATUS_SUCCESS.
//  o  the new lookahead character is the character following the literal.
//
// error-conditions:
//  o  lexer->lexeme.string contains the part of the literal before the
//     offending character, followed by a C string terminator (ASCII NUL).
//  o  lexer->lexeme.length contains the length of lexer->lexeme.string.
//  o  lexer->token contains TOKEN_ILLEGAL_CHARACTER.
//  o  lexer->lexkey contains 0.
//  o  lexer->offending_char contains the offending character.
//  o  lexer->offending_char_pos contains the position of the offending char.
//  o  lexer->status contains
//     - ET_LEXER_STATUS_LITERAL_TOO_LONG if maximum length is exceeded,
//     - ET_LEXER_STATUS_STRING_NOT_DELIMITED if EOF is reached,
//     - ET_LEXER_STATUS_ILLEGAL_CHARACTER if illegal characters are found.
//  o  characters in the input stream are skipped until a matching closing
//     quotation mark delimiter or EOF is found.
//  o  the new lookahead character is the character following the literal.

static fmacro uchar_t get_quoted_literal(et_lexer_s *lexer) {
    uchar_t ch, delimiter_ch;
    
#ifndef PRIV_FUNCS_DONT_CHECK_NULL_PARAMS
    if (lexer == NULL) return (uchar_t)0;
#endif
    
    lexer->lexeme.length = 0;
    lexer->lexkey = ET_HASH_INITIAL;
    
    // copy opening delimiter
    ch = readchar();
    delimiter_ch = ch;
    lexer->lexeme.string[lexer->lexeme.length] = ch;
    lexer->lexkey = ET_HASH_NEXT_CHAR(lexer->lexkey, ch);
    lexer->lexeme.length++;
    ch = nextchar();
    
    while ((ch != delimiter_ch) && (IS_NOT_CONTROL(ch)) &&
           (index < E2F_MAX_STRING_LENGTH) && (NOT_EOF(lexer))) {
        
        // check for escaped chars
        if (ch != BACKSLASH)
            ch = readchar();
        else // backslash escaped char
            ch = get_escaped_char(lexer);
        
        // copy the char into lexeme string
        lexer->lexeme.string[lexer->lexeme.length] = ch;
        lexer->lexkey = ET_HASH_NEXT_CHAR(lexer->lexkey, ch);
        lexer->lexeme.length++;
        
        // prepare for next
        ch = nextchar();
    } // end while    
    
    if (ch == delimiter_ch) {
        // copy closing delimiter
        ch = readchar();
        lexer->lexeme.string[lexer->lexeme.length] = ch;
        lexer->lexkey = ET_HASH_NEXT_CHAR(lexer->lexkey, ch);
        lexer->lexeme.length++;
        
        // terminate lexeme string
        lexer->lexeme.string[lexer->lexeme.length] = CSTRING_TERMINATOR;
        lexer->lexkey = ET_HASH_FINAL(lexer->lexkey);
        
        // pass back token and lexeme key
        lexer->token = TOKEN_LITERAL;
        
        // set status
        if (status != KVS_STATUS_ALLOCATION_FAILED)
            lexer->status = ET_LEXER_STATUS_SUCCESS;
        else
            lexer->status = ET_LEXER_STATUS_ALLOCATION_FAILED;
        
        // return lookahead char
        return nextchar();
    }
    else { // error
        lexer->token = TOKEN_ILLEGAL_CHARACTER;
        lexer->lexkey = 0;
        lexer->offending_char = ch;
        lexer->offending_char_pos = lexer->current_pos;
        
        // terminate lexeme string
        lexer->lexeme.string[lexer->lexeme.length] = CSTRING_TERMINATOR;
        
        // set status
        if (index >= ET_MAX_STRING_LENGTH)
            lexer->status = ET_LEXER_STATUS_LITERAL_TOO_LONG;
        else if (EOF_REACHED(lexer))
            lexer->status = ET_LEXER_STATUS_STRING_NOT_DELIMITED;
        else
            lexer->status = ET_LEXER_STATUS_ILLEGAL_CHARACTER;
        
        // skip past string literal
        while ((ch != delimiter_ch) && (NOT_EOF(lexer))) {
            if (ch != BACKSLASH)
                ch = readchar();
            else // backslash escaped char
                ch = get_escaped_char(lexer);
            ch = nextchar();
        } // end while
        
        // return lookahead char
        return ch;
    } // end if
} // end get_quoted_literal


// ---------------------------------------------------------------------------
// private function:  get_escaped_char(lexer)
// ---------------------------------------------------------------------------
//
// Reads an  escaped character sequence  from the input stream of <lexer>  and
// returns the character represented by the escape sequence.
//
// pre-conditions:
//  o  lexer is an initialised lexer object
//  o  current character is *assumed* to be backslash
//
// post-conditions:
//  if the assumed backslash starts an escape sequence
//  o  the current character is the last character in the escape sequence
//  o  the lookahead character is the character following the escape sequence
//  o  line and coloumn counters are updated
//
//  if the assumed backslash does not start an escape sequence
//  o  current character, lookahead character, line and coloumn counter
//     remain unchanged
//
// return-value:
//  if the assumed backslash starts an escape sequence
//  o  the escaped character is returned
//
//  if the assumed backslash does not start an escape sequence
//  o  a backslash is returned

static fmacro uchar_t get_escaped_char(et_lexer_s *lexer) {
    uchar_t ch, nextch;
    bool escape_sequence_found = false;
    
#ifndef PRIV_FUNCS_DONT_CHECK_NULL_PARAMS
    if (lexer == NULL) return (uchar_t)0;
#endif
    
    // must NOT consume current character
    // simply assume that it is backslash
    ch = BACKSLASH;
    
    // get the lookahead character
    nextch = nextchar();
    
    switch (nextch) {
        case DOUBLE_QUOTE :
        case SINGLE_QUOTE :
            escape_sequence_found = true;
            ch = nextchar();
            break;
        case DIGIT_ZERO :
            escape_sequence_found = true;
            ch = ASCII_NUL;
            break;
        case LOWERCASE_N :
            escape_sequence_found = true;
            ch = LINEFEED;
            break;
        case LOWERCASE_R :
            escape_sequence_found = true;
            ch = CARRIAGE_RETURN;
            break;
        case LOWERCASE_T :
            escape_sequence_found = true;
            ch = TAB;
            break;
        case BACKSLASH :
            escape_sequence_found = true;
            ch = BACKSLASH;
    } // end switch
    
    // consume current character only if escape sequence was found
    if (escape_sequence_found)
        readchar();
    
    return ch;
} // end get_escaped_char


// ---------------------------------------------------------------------------
// private function:  add_lexeme_to_lextab(lexer)
// ---------------------------------------------------------------------------
//
// Adds the lexer's current lexeme to its accociated lexeme table.
//
// pre-conditions:
//  o  lexer is an initialised lexer object.
//  o  lexer->lextab is an initialised KVS table object.
//  o  lexer->lexeme.string is a properly terminated C string containing the
//     lexeme of the current symbol.
//  o  lexer->lexeme.length contains the length of lexer->lexeme.string.
//  o  lexer->lexkey contains the key for lexer->lexeme.string.
//
// post-conditions:
//  o  lexer->lexeme.string has been entered into lexer->lextab.
//  o  lexer->status contains OBJM2_LEXER_STATUS_SUCCESS.
//
// error-conditions:
//  if memmory allocation failed:
//  o  no entry has been added to lexer->lextab.
//  o  lexer->status contains ET_LEXER_STATUS_ALLOCATION_FAILED.

static fmacro void add_lexeme_to_lextab(et_lexer_s *lexer) {
    kvs_status_t status;
    
#ifndef PRIV_FUNCS_DONT_CHECK_NULL_PARAMS
    if ((lexer == NULL) || (lexer-lextab == NULL)) return;
#endif
    
    kvs_store_value(lexer->lextab,
                    lexer->lexkey,
                    lexer->lexeme.string,
                    lexer->lexeme.length, true, &status);
    
    if (status == KVS_STATUS_ALLOCATION_FAILED)
        lexer->status = status;
    
} // end add_lexeme_to_lextab


// ---------------------------------------------------------------------------
// private function:  skip_c_comment(lexer)
// ---------------------------------------------------------------------------
//
// Skips past the next closing C comment delimiter ('*/')  in the input stream
// of <lexer> and returns the character following the comment delimiter.
//
// pre-condition:
//  o  lexer is an initialised lexer object.
//  o  the  lookahead character  is  the  asterisk  of  the  opening C comment
//     delimiter at the beginning of the comment.
//
// post-conditions:
//  o  the  new lookahead character  is  the   character following the closing
//     C comment delimiter at the end of the comment.
//  o  the lexer's line and coloumn counters have been updated.

static fmacro uchar_t skip_c_comment(et_lexer_s *lexer) {
    
#ifndef PRIV_FUNCS_DONT_CHECK_NULL_PARAMS
    if (lexer == NULL) return (uchar_t)0;
#endif
    
    // consume the opening comment delimiter
    readchar();
    
    // skip all characters until end-of-file or '*/' is found
    while (NOT_EOF(lexer)) {
        if ((readchar() == ASTERISK) && (nextchar() == FORWARD_SLASH)) {
            // skip over terminating forward slash
            readchar();
            break;
        } // end if
    } // end while
    
    // return the lookahead character
    return nextchar();
} // end skip_c_comment


// ---------------------------------------------------------------------------
// private function:  skip_past_end_of_line(lexer)
// ---------------------------------------------------------------------------
//
// Skips past the next end-of-line in the input stream of <lexer>  and returns
// the character following end-of-line.
//
// pre-condition:
//  o  lexer is an initialised lexer object.
//
// post-conditions:
//  o  the new lookahead character is  the character following the end-of-line
//     marker.
//  o  the lexer's line and coloumn counters have been updated.

static fmacro uchar_t skip_past_end_of_line(et_lexer_s *lexer) {
    
#ifndef PRIV_FUNCS_DONT_CHECK_NULL_PARAMS
    if (lexer == NULL) return (uchar_t)0;
#endif
    
    // consume the lookahead character
    readchar();
    
    // skip all characters until end-of-line marker is found
    while ((NOT_EOF(lexer)) && (nextchar() != EOL))
        readchar();
    
    // skip past the end-of-line marker
    readchar();
    
    // return the lookahead character
    return nextchar();
} // end skip_past_end_of_line


// END OF FILE
