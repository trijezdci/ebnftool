/* EBNF tool
 *
 *  @file et_parser.c
 *  EBNF parser implementation
 *
 *  Syntax analysis for EBNF grammar files
 *
 *  Author: Benjamin Kowarsch
 *
 *  Copyright (C) 2009 Benjamin Kowarsch. All rights reserved.
 *
 *  License:
 *
 *  Redistribution  and  use  in source  and  binary forms,  with  or  without
 *  modification, are permitted provided that the following conditions are met
 *
 *  1) NO FEES may be charged for the provision of the software.  The software
 *     may  NOT  be  hosted  on websites  which  contain  advertising,  unless
 *     specific  prior  written  permission has been obtained.
 *
 *  2) Redistributions  of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *  3) Redistributions  in binary form  must  reproduce  the  above  copyright
 *     notice,  this list of conditions  and  the following disclaimer  in the
 *     documentation and other materials provided with the distribution.
 *
 *  4) Neither the author's name nor the names of any contributors may be used
 *     to endorse  or  promote  products  derived  from this software  without
 *     specific prior written permission.
 *
 *  5) Where this list of conditions  or  the following disclaimer, in part or
 *     as a whole is overruled  or  nullified by applicable law, no permission
 *     is granted to use the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,  BUT NOT LIMITED TO,  THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY  AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT  SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE  FOR  ANY  DIRECT,  INDIRECT,  INCIDENTAL,  SPECIAL,  EXEMPLARY,  OR
 * CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE,  DATA,  OR PROFITS; OR BUSINESS
 * INTERRUPTION)  HOWEVER  CAUSED  AND ON ANY THEORY OF LIABILITY,  WHETHER IN
 * CONTRACT,  STRICT LIABILITY,  OR TORT  (INCLUDING NEGLIGENCE  OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,  EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *  
 *  Version history:
 *
 *   1.00   2009-09-25   BK   new file
 */


#include <stdio.h>
#include <stdint.h>

#include "et_tokens.h"
#include "et_parser.h"


// --------------------------------------------------------------------------
// Tokenset type
// --------------------------------------------------------------------------

typedef uint_fast32_t et_tokenset_t;

#if (ET_NUMBER_OF_TOKENS > 32)
#error "Fatal error: number of tokens must not exceed 32."
#endif


// --------------------------------------------------------------------------
// macro:  SET( token )
// --------------------------------------------------------------------------
//
// Returns a set with member <token>

#define SET(_token)   (1 << (_token))


// --------------------------------------------------------------------------
// macro:  IN_SET( token, set )
// --------------------------------------------------------------------------
//
// Returns true if <token> is member of tokenset <set>,  otherwise false.

#define IN_SET(_token, _set)   (((1 << (_token)) & (_set)) != 0)


// --------------------------------------------------------------------------
// macro:  UNION( set1, set2 )
// --------------------------------------------------------------------------
//
// Returns the union of tokensets <set1> and <set2>.

#define UNION(_set1, _set2)   ((_set1) | (_set2))


// --------------------------------------------------------------------------
// FIRST sets
// --------------------------------------------------------------------------

#define FIRST_grammar ( \
    1 << TOKEN_UPPERCASE_NAME + \
    1 << TOKEN_LOWERCASE_NAME )

#define FIRST_terminal_definition ( \
    1 << TOKEN_UPPERCASE_NAME )

#define FIRST_non_terminal_definition ( \
    1 << TOKEN_LOWERCASE_NAME )

#define FIRST_terminal_expression ( \
    1 << TOKEN_UPPERCASE_NAME + \
    1 << TOKEN_LITERAL + \
    1 << TOKEN_OPENING_PARENTHESIS )

#define FIRST_terminal_factor ( \
    1 << TOKEN_UPPERCASE_NAME + \
    1 << TOKEN_LITERAL + \
    1 << TOKEN_OPENING_PARENTHESIS )

#define FIRST_terminal_group ( \
    1 << TOKEN_OPENING_PARENTHESIS )

#define FIRST_expression ( \
    1 << TOKEN_UPPERCASE_NAME + \
    1 << TOKEN_LOWERCASE_NAME + \
    1 << TOKEN_LITERAL + \
    1 << TOKEN_OPENING_PARENTHESIS )

#define FIRST_factor ( \
    1 << TOKEN_UPPERCASE_NAME + \
    1 << TOKEN_LOWERCASE_NAME + \
    1 << TOKEN_LITERAL + \
    1 << TOKEN_OPENING_PARENTHESIS )

#define FIRST_group ( \
    1 << TOKEN_OPENING_PARENTHESIS )

#define FIRST_name ( \
    1 << TOKEN_UPPERCASE_NAME + \
    1 << TOKEN_LOWERCASE_NAME )


// --------------------------------------------------------------------------
// FOLLOW sets
// --------------------------------------------------------------------------

#define FOLLOW_grammar ( \
    1 << TOKEN_END_OF_FILE )

#define FOLLOW_terminal_definition ( \
    1 << TOKEN_LOWERCASE_NAME + \
    1 << TOKEN_END_OF_FILE )

#define FOLLOW_non_terminal_definition ( \
    1 << TOKEN_UPPERCASE_NAME + \
    1 << TOKEN_END_OF_FILE )

#define FOLLOW_terminal_expression ( \
    1 << TOKEN_SEMICOLON )

#define FOLLOW_terminal_factor ( \
    1 << TOKEN_UPPERCASE_NAME + \
    1 << TOKEN_LITERAL + \
    1 << TOKEN_OPENING_PARENTHESIS + \
    1 << TOKEN_VERTICAL_BAR + \
    1 << TOKEN_SEMICOLON )

#define FOLLOW_terminal_group ( \
    1 << TOKEN_UPPERCASE_NAME + \
    1 << TOKEN_LITERAL + \
    1 << TOKEN_OPENING_PARENTHESIS + \
    1 << TOKEN_QUESTION_MARK + \
    1 << TOKEN_PLUS + \
    1 << TOKEN_ASTERISK + \
    1 << TOKEN_VERTICAL_BAR + \
    1 << TOKEN_SEMICOLON )

#define FOLLOW_expression ( \
    1 << TOKEN_SEMICOLON )

#define FOLLOW_factor ( \
    1 << TOKEN_UPPERCASE_NAME + \
    1 << TOKEN_LOWERCASE_NAME + \
    1 << TOKEN_LITERAL + \
    1 << TOKEN_OPENING_PARENTHESIS + \
    1 << TOKEN_VERTICAL_BAR + \
    1 << TOKEN_SEMICOLON )

#define FOLLOW_group ( \
    1 << TOKEN_UPPERCASE_NAME + \
    1 << TOKEN_LOWERCASE_NAME + \
    1 << TOKEN_LITERAL + \
    1 << TOKEN_OPENING_PARENTHESIS + \
    1 << TOKEN_QUESTION_MARK + \
    1 << TOKEN_PLUS + \
    1 << TOKEN_ASTERISK + \
    1 << TOKEN_VERTICAL_BAR + \
    1 << TOKEN_SEMICOLON )

#define FOLLOW_name ( \
    1 << TOKEN_UPPERCASE_NAME + \
    1 << TOKEN_LOWERCASE_NAME + \
    1 << TOKEN_LITERAL + \
    1 << TOKEN_OPENING_PARENTHESIS + \
    1 << TOKEN_QUESTION_MARK + \
    1 << TOKEN_PLUS + \
    1 << TOKEN_ASTERISK + \
    1 << TOKEN_VERTICAL_BAR + \
    1 << TOKEN_SEMICOLON )


// --------------------------------------------------------------------------
// Symbol key type
// --------------------------------------------------------------------------

typedef unsigned int et_symbol_key_t;


// --------------------------------------------------------------------------
// Symbol table entry type
// --------------------------------------------------------------------------

typedef struct /* et_symbol_table_entry_s */ {
    et_symbol_type_t type;
     et_symbol_key_t key;
    
} et_symbol_table_entry_s;

// --------------------------------------------------------------------------
// Symbol type
// --------------------------------------------------------------------------

typedef struct /* et_symbol_s */ {
           et_token_t token;
      et_symbol_key_t key;
    et_lexer_status_t status;
} et_symbol_s;


// --------------------------------------------------------------------------
// Parser state type
// --------------------------------------------------------------------------

typedef struct /* et_parser_s */ {
     et_lexer_t lexer;
    et_symbol_s lookahead_sym;
    et_symbol_s current_sym;
    unsigned int error_count;
} t_parser_s;


// --------------------------------------------------------------------------
// private function:  getsym( parser_state )
// --------------------------------------------------------------------------
//
// Reads a  new symbol  from the input stream,  stores the previous lookahead
// symbol as the  current symbol,  stores the new symbol as lookahead symbol,
// then returns the current symbol's token.

static inline et_token_t getsym(et_parser_s *p) {
    p->current_sym = p->lookahead_sym;
    p->lookahead_sym.token = et_lexer_getsym(p->lexer,
                                             &p->lookahead_sym.key,
                                             &p->lookahead_sym.status);
    return p->current_sym.token;
} // end getsym


// --------------------------------------------------------------------------
// private function:  lookahead( parser_state )
// --------------------------------------------------------------------------
//
// Returns the token of the current lookahead symbol.  This function does not
// read from the input stream.  Subsequent calls to this function will return
// the  same token  again.  To read  further symbols  from the  input stream,
// function getsym must be used.

static inline et_token_t lookahead(et_parser_s *p) {
    return p->lookahead_sym.token;
} // end lookahead


// --------------------------------------------------------------------------
// private function:  et_parse( parser_state )
// --------------------------------------------------------------------------

static void et_parse(et_parser_s *p) {
    et_token_t token;
    
    // start symbol: grammar
    token = et_grammar(p);
    
    if NOT (IN_SET(token, FOLLOW_grammar)) {
        p->error_count++;
        // report error
        mismatch(p, token, FOLLOW_grammar);
        // skip to recovery symbol
        token = skip_to(p, FOLLOW_grammar);
    } // end if
    
    return;
} // end et_parse


// --------------------------------------------------------------------------
// private function:  et_grammar( parser_state )
// --------------------------------------------------------------------------
//
// Implements production grammar:
//
// grammar :
//     definition* ;

static et_token_t et_grammar(et_parser_s *p) {
    et_token_t token;

    token = lookahead(p);
    
    while (IN_SET(token, FIRST_grammar))
        token = et_definition(p);
    
    return token;
} // end et_grammar


// --------------------------------------------------------------------------
// private function:  et_definition( parser_state )
// --------------------------------------------------------------------------
//
// Implements production definition:
//
// definition :
//     ( terminal_definition | non_terminal_definition ) ";" ;

static et_token_t et_definition(et_parser_s *p) {
    et_token_t token;
    
    token = lookahead(p);
    
    switch(token) {
        case TOKEN_UPPERCASE_NAME :
            token = et_terminal_definition(p);
            break;
        case TOKEN_LOWERCASE_NAME :
            token = et_non_terminal_definition(p);
            break;
        default :
            abort();
    } // end switch
    
    if (token == TOKEN_SEMICOLON) {
        getsym(p); // consume
        token = lookahead(p);
    }
    else {
        p->error_count++;
        // report error
        mismatch(p, token, SET(TOKEN_SEMICOLON));
        // skip to recovery symbol
        token = skip_to(p, SET(TOKEN_SEMICOLON));
    } // end if
    
    return token;
} // end et_definition


// --------------------------------------------------------------------------
// private function:  et_terminal_definition( parser_state )
// --------------------------------------------------------------------------
//
// Implements production terminal_definition:
//
// terminal_definition :
//     UPPERCASE_NAME ( ":" | "=" ) terminal_expression ;

static et_token_t et_terminal_definition(et_parser_s *p) {
    et_token_t token;
    et_tokenset_t first;
    et_symbol_key_t lexkey;
    et_symt_entry_t entry;
    
    getsym(p); // consume    
    token = lookahead(p);
    
    if (token == TOKEN_COLON) {
        
        // new terminal symbol
        if (p->error_count == 0)
            et_ast_new_symbol(ET_AST_SYMTYPE_TERMINAL,
                              p->current_sym.key, NULL);
        
        getsym(p); // consume
        token = lookahead(p);
    }
    else if (token == TOKEN_EQUALS) {

        // new terminal alias
        if (p->error_count == 0)
            et_ast_new_symbol(ET_AST_SYMTYPE_TERMINAL_ALIAS,
                              p->current_sym.key, NULL);
        
        getsym(p); // consume
        token = lookahead(p);
    }
    else {
        // syntax error
        p->error_count++
        // report error
        first = SET(TOKEN_COLON) + SET(TOKEN_EQUALS);
        mismatch(p, token, first);
        // skip to recovery symbol
        token = skip_to(p, FOLLOW_terminal_definition);
    } // end if
    
    if (IN_SET(token, FIRST_terminal_expression))
        token = et_terminal_expression(p);
    else {
        p->error_count++;
        // report error
        mismatch(p, token, FIRST_terminal_expression);
        // skip to recovery symbol
        token = skip_to(p, FOLLOW_terminal_expression);
    } // end if
    
    return token;
} // end et_terminal_definition


// --------------------------------------------------------------------------
// private function:  et_non_terminal_definition( parser_state )
// --------------------------------------------------------------------------
//
// Implements production non_terminal_definition:
//
// non_terminal_definition :
//     LOWERCASE_NAME ( ":" expression | "=" name ) ;

static et_token_t et_non_terminal_definition(et_parser_s *p) {
    et_token_t token;
    et_tokenset_t first;
    
    // consume first token
    getsym(p);
    
    // get next token
    token = lookahead(p);
    
    if (token == TOKEN_COLON) {

        // new non-terminal symbol
        if (p->error_count == 0)
            et_ast_new_symbol(ET_AST_SYMTYPE_NONTERMINAL,
                              p->current_sym.key, NULL);

        getsym(p); // consume
        token = lookahead();
        
        if (IN_SET(token, FIRST_expression))
            token = et_expression(p);
        else {
            p->error_count++;
            // report error
            mismatch(p, token, FIRST_expression);
            // skip to recovery symbol
            token = skip_to(p, FOLLOW_non_terminal_definition);
        } // end if
    }
    else if (token == TOKEN_EQUALS) {

        // new non-terminal alias
        if (p->error_count == 0)
            et_ast_new_symbol(ET_AST_SYMTYPE_NONTERMINAL_ALIAS,
                              p->current_sym.lexkey, NULL);
        
        getsym(p); // consume
        token = lookahead(p);
        
        if (token == UPPERCASE_NAME) {
            getsym(p); // consume
            // to do: get name
            token = lookahead(p);
        }
        else if (token = LOWERCASE_NAME) {
            getsym(p); // consume
            // to do: get name
            token = lookahead(p);
        }
        else {
            p->error_count++;
            // report error
            mismatch(p, token, FIRST_name);
            // skip to recovery symbol
            token = skip_to(p, FOLLOW_non_terminal_definition);
        } // end if
    }
    else /* any other token */ {
        p->error_count++;
        // report error
        first = SET(TOKEN_COLON) + SET(TOKEN_EQUALS);
        mismatch(p, token, first);
        // skip to recovery symbol
        token = skip_to(p, FOLLOW_non_terminal_definition);
    } // end if
        
    return token;
} // end et_non_terminal_definition


// --------------------------------------------------------------------------
// private function:  et_terminal_expression( parser_state )
// --------------------------------------------------------------------------
//
// Implements production terminal_expression:
//
// terminal_expression :
//     terminal_factor+ ( "|" terminal_factor+ )* ;

static et_token_t et_terminal_expression(et_parser_s *p) {
    et_token_t token;
    et_tokenset_t first;
    
    token = lookahead(p);
    
    if (IN_SET(token, FIRST_terminal_factor)) {
        while (IN_SET(token, FIRST_terminal_factor)) {
            token = et_terminal_factor(p);
        } // end while
    }
    else {
        p->error_count++;
        // report error
        mismatch(p, token, FIRST_terminal_factor);
        // skip to recovery symbol
        token = skip_to(p, FOLLOW_terminal_factor);
    } // end if
    
    first = SET(TOKEN_VERTICAL_BAR);
    while (IN_SET(token, first)) {
        getsym(p);
        token = lookahead(p);
        
        et_ast_add_term(0, NULL);
        
        if (IN_SET(token, FIRST_terminal_factor)) {
            while (IN_SET(token, FIRST_terminal_factor)) {
                token = et_terminal_factor(p);
            } // end while
        }
        else {
            p->error_count++;
            // report error
            mismatch(p, token, FIRST_terminal_factor);
            // skip to recovery symbol
           token = skip_to(p, FOLLOW_terminal_factor);
        } // end if
    } // end while
    
    return token;
} // et_terminal_expression
    

// --------------------------------------------------------------------------
// private function:  et_terminal_factor( parser_state )
// --------------------------------------------------------------------------
//
// Implements production terminal_factor:
//
// terminal_factor :
//     ( UPPERCASE_NAME | LITERAL ( ".." LITERAL )? | terminal_group )
//     ( "?" | "+" | "*" )? ;

static et_token_t et_terminal_factor(et_parser_s *p) {
    et_token_t token;
    
    token = lookahead(p);
    
    if (token == TOKEN_UPPERCASE_NAME) {
        getsym(p); // consume
        token = lookahead(p);
        
        // first add the new symbol to the symbol table
        et_ast_new_symbol(ET_AST_SYMTYPE_TERMINAL,
                          p->current_sym.key, NULL);
        // then add the symbol as a factor to the parent symbol
        et_ast_add_factor(0, p->current_sym.key, 0, NULL);
    }
    else if (token == TOKEN_LITERAL) {
        getsym(p); // consume
        token = lookahead(p);
        
        et_ast_add_factor(0, 0, 0, NULL);
    }
    else if (IN_SET(token, FIRST_terminal_group)) {
        token = et_terminal_group(p);
    }
    else {
        p->error_count++;
        // report error
        mismatch(p, token, FIRST_terminal_factor);
        // skip to recovery symbol
        token = skip_to(p, FOLLOW_terminal_factor);
    } // end if
    
    if (token == TOKEN_QUESTION_MARK) {
        getsym(p); // consume
        token = lookahead(p);
    }
    else if (token == TOKEN_PLUS) {
        getsym(p); // consume
        token = lookahead(p);
    }
    else if (token == TOKEN_ASTERISK) {
        getsym(p); // consume
        token = lookahead(p);
    } // end
    
    return token;
} // end et_terminal_factor


// --------------------------------------------------------------------------
// private function:  et_terminal_group( parser_state )
// --------------------------------------------------------------------------
//
// Implements production terminal_group:
//
// terminal_group :
//     "(" terminal_expression ")" ;

static et_token_t et_terminal_group(et_parser_s *p) {
    et_token_t token;
    
    getsym(p); // consume
    token = lookahead(p);
    
    if (IN_SET(token, FIRST_terminal_expression)) {
        token = et_terminal_expression(p);
    }
    else {
        p->error_count++;
        // report error
        mismatch(p, token, FIRST_terminal_expression);
        // skip to recovery symbol
        token = skip_to(p, FOLLOW_terminal_group);
    } // end if
    
    return token;
} // end et_terminal_group


// --------------------------------------------------------------------------
// private function:  et_expression( parser_state )
// --------------------------------------------------------------------------
//
// Implements production expression:
//
// expression :
//     factor+ ( "|" factor+ )* ;

static et_token_t et_expression(et_parser_s *p) {
    et_token_t token;
    et_tokenset_t first;
    
    token = lookahead(p);
    
    if (IN_SET(token, FIRST_factor)) {
        while (IN_SET(token, FIRST_factor)) {
            token = et_factor(p);
        } // end while
    }
    else {
        p->error_count++;
        // report error
        mismatch(p, token, FIRST_factor);
        // skip to recovery symbol
        token = skip_to(p, FOLLOW_factor);
    } // end if
    
    first = SET(TOKEN_VERTICAL_BAR);
    while (IN_SET(token, first)) {
        getsym(p); // consume
        token = lookahead(p);
        
        if (IN_SET(token, FIRST_factor)) {
            while (IN_SET(token, FIRST_factor)) {
                token = et_factor(p);
            } // end while
        }
        else {
            p->error_count++;
            // report error
            mismatch(p, token, FIRST_factor);
            // skip to recovery symbol
            token = skip_to(p, FOLLOW_factor);
        } // end if
    } // end while
    
    return token;
} // et_expression


// --------------------------------------------------------------------------
// private function:  et_factor( parser_state )
// --------------------------------------------------------------------------
//
// Implements production factor:
//
// factor :
//     ( name | LITERAL ( ".." LITERAL )? | group ) ( "?" | "+" | "*" )? ;

static et_token_t et_factor(et_parser_s *p) {
    et_token_t token;
    
    token = lookahead(p);
    
    if (token == TOKEN_UPPERCASE_NAME) {
        getsym(p); // consume
        token = lookahead(p);
    }
    else if (token == TOKEN_LOWERCASE_NAME) {
        getsym(p); // consume
        token = lookahead(p);
    }
    else if (token == TOKEN_LITERAL) {
        getsym(p); // consume
        token = lookahead(p);
    }
    else if (IN_SET(token, FIRST_group)) {
        token = e2f_group(p);
    }
    else {
        p->error_count++;
        // report error
        mismatch(p, token, FIRST_factor);
        // skip to recovery symbol
        token = skip_to(FOLLOW_factor);
    } // end if
    
    if (token == TOKEN_QUESTION_MARK) {
        getsym(p); // consume
        token = lookahead(p);
    }
    else if (token == TOKEN_PLUS) {
        getsym(); // consume
        token = lookahead(p);
    }
    else if (token == TOKEN_ASTERISK) {
        getsym(p); // consume
        token = lookahead(p);
    } // end
    
    return token;
} // end et_factor


// --------------------------------------------------------------------------
// private function:  et_group( parser_state )
// --------------------------------------------------------------------------
//
// Implements production group:
//
// group :
//     "(" expression ")" ;

static et_token_t et_group(et_parser_s *p) {
    et_token_t token;
    
    getsym(p); // consume
    token = lookahead(p);
    
    if (IN_SET(token, FIRST_expression)) {
        token = et_expression(p);
    }
    else {
        p->error_count++;
        // report error
        mismatch(p, token, FIRST_expression);
        // skip to recovery symbol
        token = skip_to(p, FOLLOW_group);
    } // end if
    
    return token;
} // end et_group


// --------------------------------------------------------------------------
// private function:  mismatch( p, found_token, expected_set )
// --------------------------------------------------------------------------
//
// Prints an error message to stderr.

static void mismatch(et_parser_s *p,
                      et_token_t found_token,
                   et_tokenset_t expected_set) {
    
    unsigned int row = 0, col = 0, index, token_count;
    et_token_t token_list[ET_NUMBER_OF_TOKENS];
    et_token_t token;
    
    if (expected_set == 0) return;
    
    // create token list from expected_set
    index = 0; token = 0;
    while (token < ET_NUMBER_OF_TOKENS) {
        if (IN_SET(token, expected_set)) {
            token_list[index] = token;
            index++;
        } // end if
        token++;
    } // end while
    
    if (index == 0) return;
    
    // remember number of tokens in list
    token_count = index;
    
    // get position of offending symbol
    et_lexer_getpos(p->lexer, &row, &col, NULL);

    // print name of offending symbol and its position
    fprintf(stderr, "syntax error in line:%i, col:%i, found %s, expected ",
           row, col, et_token_name(found_token));
    
    // print list of names of expected tokens
    index = 0;
    while (index < token_count) {
        token = token_list[index];
        fprintf(stderr, "%s", et_token_name(token));
        if (index + 2 < token_count)
            fprintf(stderr, ", ");
        else if (index + 2 == token_count)
            fprintf(stderr, " or ");
        else
            fprintf(stderr, "\n");
        index++;
    } // end while
    
    return;
} // end mismatch


// --------------------------------------------------------------------------
// private function:  skip_to( parser_state, recovery_set )
// --------------------------------------------------------------------------
//
// Skips tokens until lookahead token is a member of set <recovery_set>.

static et_token_t skip_to(et_parser_s *p,
                        et_tokenset_t recovery_set) {
    e2f_token_t token;
    
    token = lookahead(p);
    
    while NOT (IN_SET(token, recovery_set) {
        getsym(p); // skip
        token = lookahead(p);
    } // end while
    
    return token;
} // end skip_to


// END OF FILE
