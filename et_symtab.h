/* EBNF tool
 *
 *  @file et_symtab.h
 *  symbol table interface
 *
 *  Syntax analysis for EBNF grammar files
 *
 *  Author: Benjamin Kowarsch
 *
 *  Copyright (C) 2009 Benjamin Kowarsch. All rights reserved.
 *
 *  License:
 *
 *  Redistribution  and  use  in source  and  binary forms,  with  or  without
 *  modification, are permitted provided that the following conditions are met
 *
 *  1) NO FEES may be charged for the provision of the software.  The software
 *     may  NOT  be  hosted  on websites  which  contain  advertising,  unless
 *     specific  prior  written  permission has been obtained.
 *
 *  2) Redistributions  of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *
 *  3) Redistributions  in binary form  must  reproduce  the  above  copyright
 *     notice,  this list of conditions  and  the following disclaimer  in the
 *     documentation and other materials provided with the distribution.
 *
 *  4) Neither the author's name nor the names of any contributors may be used
 *     to endorse  or  promote  products  derived  from this software  without
 *     specific prior written permission.
 *
 *  5) Where this list of conditions  or  the following disclaimer, in part or
 *     as a whole is overruled  or  nullified by applicable law, no permission
 *     is granted to use the software.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING,  BUT NOT LIMITED TO,  THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY  AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT  SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE  FOR  ANY  DIRECT,  INDIRECT,  INCIDENTAL,  SPECIAL,  EXEMPLARY,  OR
 * CONSEQUENTIAL  DAMAGES  (INCLUDING,  BUT  NOT  LIMITED  TO,  PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES;  LOSS OF USE,  DATA,  OR PROFITS; OR BUSINESS
 * INTERRUPTION)  HOWEVER  CAUSED  AND ON ANY THEORY OF LIABILITY,  WHETHER IN
 * CONTRACT,  STRICT LIABILITY,  OR TORT  (INCLUDING NEGLIGENCE  OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,  EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *  
 *  Version history:
 *
 *   1.00   2009-09-25   BK   new file
 */


#ifndef ET_SYMTAB_H
#define ET_SYMTAB_H

#include "KVS.h"


// ---------------------------------------------------------------------------
// Symbol table type
// ---------------------------------------------------------------------------

typedef kvs_table_t et_symtab_t;


// ---------------------------------------------------------------------------
// Symbol key type
// ---------------------------------------------------------------------------

typedef kvs_key_t et_symkey_t;


// ---------------------------------------------------------------------------
// Symbol data type
// ---------------------------------------------------------------------------

typedef kvs_data_t et_symbol_t;


// ---------------------------------------------------------------------------
// function macro:  et_new_symbol_table( status )
// ---------------------------------------------------------------------------

#define et_new_symbol_table(_status) \
    kvs_new_table(0, _status)


// ---------------------------------------------------------------------------
// function macro:  et_add_symbol( table, symkey, symbol, status )
// ---------------------------------------------------------------------------

#define et_add_symbol(_table, _symkey, _symbol, _status) \
    kvs_store_reference(_table, _symkey, _symbol, sizeof(et_symbol_s), false, _status)


// ---------------------------------------------------------------------------
// function macro:  et_get_symbol( table, symkey, status )
// ---------------------------------------------------------------------------

#define et_get_symbol(_table, _symkey, _status) \
    kvs_reference_for_key(_table, _symkey, _status)


// ---------------------------------------------------------------------------
// function macro:  et_remove_symbol( table, symkey, status )
// ---------------------------------------------------------------------------

#define et_remove_symbol(_table, _symkey, _status) \
    { kvs_release_entry(_table, _symkey, _status); \
      kvs_remove_entry(_table, _symkey, _status); }


// ---------------------------------------------------------------------------
// function macro:  et_dispose_symbol_table( table, status )
// ---------------------------------------------------------------------------

#define et_dispose_symbol_table(_table, _status) \
    kvs_dispose_table(_table, _status)


#endif /* ET_SYMTAB_H */

// END OF FILE